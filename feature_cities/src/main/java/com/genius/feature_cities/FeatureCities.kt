package com.genius.feature_cities

import com.genius.repository.IFeatureCitiesRepository
import com.genius.repository.di.CityResponse

class FeatureCities(private val repository: IFeatureCitiesRepository) {

    suspend fun loadCities(): List<CityResponse> = repository.loadAllCities().sortedBy { it.title }
}