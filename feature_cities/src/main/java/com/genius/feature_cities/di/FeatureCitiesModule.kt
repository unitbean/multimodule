package com.genius.feature_cities.di

import com.genius.feature_cities.FeatureCities
import com.genius.repository.IFeatureCitiesRepository
import com.genius.repository.RepositoryDI
import dagger.Module
import dagger.Provides

@Module
class FeatureCitiesModule {

    @Provides
    @CitiesScope
    fun provideCitiesRepository(): IFeatureCitiesRepository {
        return RepositoryDI.repositoryComponent.getFeatureCitiesRepository()
    }

    @Provides
    @CitiesScope
    fun provideFeatureCities(repository: IFeatureCitiesRepository): FeatureCities {
        return FeatureCities(repository)
    }
}