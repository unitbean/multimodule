package com.genius.joke.di

import com.genius.joke.FeatureRandomJoke
import com.genius.repository.IFeatureRandomJokeRepository
import com.genius.repository.RepositoryDI
import dagger.Module
import dagger.Provides

@Module
class FeatureJokeModule {

    @Provides
    @FeatureRandomJokeScope
    fun provideFeatureRandomJoke(repository: IFeatureRandomJokeRepository): FeatureRandomJoke {
        return FeatureRandomJoke(repository)
    }

    @Provides
    @FeatureRandomJokeScope
    fun provideRepository(): IFeatureRandomJokeRepository {
        return RepositoryDI.repositoryComponent.getFeatureRandomJokeRepository()
    }
}