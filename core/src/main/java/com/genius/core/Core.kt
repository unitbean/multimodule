package com.genius.core

import android.annotation.SuppressLint
import android.content.Context

@SuppressLint("StaticFieldLeak")
object Core {
    lateinit var context: Context
}

fun <T>MutableList<T>.renew(list: Collection<T>): MutableList<T> {
    clear()
    addAll(list)
    return this
}
