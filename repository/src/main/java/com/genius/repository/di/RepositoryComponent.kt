package com.genius.repository.di

import com.genius.repository.IFeatureBasketRepository
import com.genius.repository.IFeatureCitiesRepository
import com.genius.repository.IFeatureRandomJokeRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    BaseRepositoryModule::class,
    FeatureRandomJokeRepositoryModule::class,
    FeatureBasketRepositoryModule::class,
    FeatureCitiesRepositoryModule::class])
interface RepositoryComponent {
    fun getFeatureRandomJokeRepository(): IFeatureRandomJokeRepository
    fun getFeatureBasketRepository(): IFeatureBasketRepository
    fun getFeatureCitiesRepository(): IFeatureCitiesRepository
}