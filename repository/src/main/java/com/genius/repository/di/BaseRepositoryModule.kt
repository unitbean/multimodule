package com.genius.repository.di

import android.content.Context
import com.genius.core.Core
import com.genius.repository.database.ItemsDatabase
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class BaseRepositoryModule {

    @Provides
    @Singleton
    fun provideContext(): Context = Core.context

    @Provides
    @Singleton
    fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(45, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    @Provides
    @Singleton
    fun getNorrisFactsApi(): NorrisFactsApi {
        return Retrofit.Builder()
            .baseUrl(NorrisFactsApi.SERVER)
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NorrisFactsApi::class.java)
    }

    @Provides
    @Singleton
    fun getItemsApi(): ItemsApi {
        return Retrofit.Builder()
            .baseUrl(ItemsApi.SERVER)
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ItemsApi::class.java)
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Context): ItemsDatabase {
        return ItemsDatabase.getDatabase(context)
    }
}