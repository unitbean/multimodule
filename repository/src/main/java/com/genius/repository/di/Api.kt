package com.genius.repository.di

import androidx.annotation.StringDef
import com.genius.repository.database.models.ACTIVE
import com.genius.repository.database.models.ItemStatus
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.ArrayList

interface NorrisFactsApi {
    @get:GET("$JOKES/random")
    val randomJoke: Deferred<Response<NorrisFactResponse>>

    @get:GET("$JOKES/count")
    val countOfJokes: Deferred<Response<Int>>

    @get:GET("$JOKES/categories")
    val categoriesOfJokes: Deferred<Response<List<String>>>

    @GET("$JOKES/random/{count}")
    fun randomJokes(
        @Path("count") count: Int = 20,
        @Query("limitTo") excludedCategories: Array<String>? = null
    ): Deferred<Response<List<NorrisFactResponse>>>

    companion object {
        const val SERVER = "https://api.icndb.com/"
        const val JOKES = "/jokes"
    }
}

interface ItemsApi {

    @get:GET("$VERSION_V2/category/full")
    val fullData: Deferred<BaseResponse<ResultResponse>>

    @get:GET("$VERSION/city/all")
    val allCities: Deferred<BaseResponse<List<CityResponse>>>

    companion object {
        const val VERSION = "/api/v1"
        const val VERSION_V2 = "/api/v2"
        const val SERVER = "https://xn--80adjkr6adm9b.xn--p1ai/"
    }
}

data class Response<T>(@ResponseType val type: String, val value: T) {

    @StringDef(
        ResponseType.SUCCESS,
        ResponseType.FAILURE
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class ResponseType {
        companion object {
            const val SUCCESS = "success"
            const val FAILURE = "failure"
        }
    }
}

data class BaseResponse<T>(val error: ErrorResponse? = null, val result: T? = null) {
    data class ErrorResponse(
        var success: Boolean = false,
        var code: Int = 0,
        var message: String? = null)
}

data class NorrisFactResponse(val id: Int, val joke: String, val categories: List<String>)

data class ResultResponse(val items: List<ItemResponse>? = null)

data class ItemResponse(
    var id: String = "",
    var title: String = "",
    var description: String? = null,
    var size: Double = 0.toDouble(),
    var count: Int = 0,
    var category: String? = null,
    var picture: String? = null,
    @get:ItemStatus
    var status: String = ACTIVE,
    var orderComponents: String? = null,
    var sortNum: Int = 0,
    var personCount: Int = 0,
    var ingredients: List<IngredientResponse> = ArrayList(),
    var isNovelty: Boolean = false,
    var isRecommended: Boolean = false
)

data class CityResponse(
    var id: String = "",
    var title: String = "",
    var code: Int = 0,
    var phone: String? = null,
    var deliveryInfo: String? = null,
    var amountForFreeDelivery: Int? = null,
    var costOfDelivery: Int? = null
)

data class IngredientResponse(
    var id: String = "",
    val title: String? = null,
    val prices: List<PriceResponse> = ArrayList(),
    val availableCities: List<String> = ArrayList(),
    val availableEverywhere: Boolean = false)


class PriceResponse {
    var code: Int = 0
    var price: Double = 0.toDouble()
}

