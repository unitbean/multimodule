package com.genius.repository.di

import com.genius.repository.FeatureCitiesRepositoryImpl
import com.genius.repository.IFeatureCitiesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FeatureCitiesRepositoryModule {

    @Provides
    @Singleton
    fun provideCitiesRepository(api: ItemsApi): IFeatureCitiesRepository {
        return FeatureCitiesRepositoryImpl(api)
    }
}