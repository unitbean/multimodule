package com.genius.repository.di

import android.content.Context
import com.genius.repository.FeatureRandomJokeRepositoryImpl
import com.genius.repository.IFeatureRandomJokeRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FeatureRandomJokeRepositoryModule {

    @Provides
    @Singleton
    fun provideFeature1Repository(api: NorrisFactsApi, context: Context): IFeatureRandomJokeRepository {
        return FeatureRandomJokeRepositoryImpl(api, context)
    }
}