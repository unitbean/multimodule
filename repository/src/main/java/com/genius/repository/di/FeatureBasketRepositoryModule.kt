package com.genius.repository.di

import com.genius.repository.FeatureBasketRepositoryImpl
import com.genius.repository.IFeatureBasketRepository
import com.genius.repository.database.ItemsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FeatureBasketRepositoryModule {

    @Provides
    @Singleton
    fun provideBasketRepository(api: ItemsApi, database: ItemsDatabase): IFeatureBasketRepository {
        return FeatureBasketRepositoryImpl(api, database)
    }
}