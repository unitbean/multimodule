package com.genius.repository

import android.content.Context
import androidx.annotation.StringRes
import com.genius.repository.database.ItemsDatabase
import com.genius.repository.database.models.ItemModel
import com.genius.repository.di.*

object RepositoryDI {

    val repositoryComponent: RepositoryComponent  = DaggerRepositoryComponent.builder()
        .build()
}

interface IFeatureRandomJokeRepository {
    suspend fun fetchJoke(): String
    fun getString(@StringRes resId: Int): String
}

interface IFeatureBasketRepository {
    suspend fun loadAllItemsAndSave()
    fun getAllItems(): List<ItemModel>
}

interface IFeatureCitiesRepository {
    suspend fun loadAllCities(): List<CityResponse>
}

class FeatureRandomJokeRepositoryImpl(private val api: NorrisFactsApi, private val context: Context) : IFeatureRandomJokeRepository {

    override suspend fun fetchJoke(): String {
        return api.randomJoke.await().value.joke
    }

    override fun getString(@StringRes resId: Int): String = context.getString(resId)
}

class FeatureBasketRepositoryImpl(private val api: ItemsApi, private val database: ItemsDatabase) : IFeatureBasketRepository {

    override suspend fun loadAllItemsAndSave() {
        val items = api.fullData.await().result?.items ?: emptyList()
        database.itemDAO.updateFromResponse(items)
    }

    override fun getAllItems(): List<ItemModel> = database.itemDAO.getAll()
}

class FeatureCitiesRepositoryImpl(private val api: ItemsApi) : IFeatureCitiesRepository {

    override suspend fun loadAllCities(): List<CityResponse> {
        return api.allCities.await().result ?: emptyList()
    }
}

