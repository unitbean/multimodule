package com.genius.repository.database.models

import androidx.annotation.StringDef

@StringDef(ACTIVE, BLOCK)
@Retention(AnnotationRetention.SOURCE)
annotation class ItemStatus

const val ACTIVE = "ACTIVE"
const val BLOCK = "BLOCK"
