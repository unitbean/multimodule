package com.genius.repository.database.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "items")
open class ItemModel {

    @PrimaryKey
    var itemId = ""
    var title: String = ""
    var description: String? = null
    var size: Double = 0.0
    var count: Int = 0
    var categoryId: String? = null
    var picture: String? = null
    var savedPicture: String? = null
    @get:ItemStatus
    var status: String = ACTIVE
    var orderComponentsId: String? = null
    var sortNum: Int = 0
    var personCount: Int = 0
    var compositeModelId: String? = null
    // для быстрой сортировки на 26 API
    var price: List<Double?> = ArrayList()
    var novelty: Boolean = false
    var isRecommended: Boolean = false

    fun compareTo(model: ItemModel, position: Int): Int {
        if (sortNum == model.sortNum) {
            if (price.size < position) {
                return -1
            } else if (model.price.size < position) {
                return 1
            }

            val price1 = price.getOrNull(position) ?: 0.0
            val price2 = model.price.getOrNull(position) ?: 0.0

            return price1.compareTo(price2)
        } else return if (sortNum > model.sortNum) {
            -1
        } else {
            1
        }
    }

    override fun toString(): String {
        return "$itemId $title categoryId:$categoryId"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ItemModel) return false

        if (itemId != other.itemId) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (size != other.size) return false
        if (count != other.count) return false
        if (categoryId != other.categoryId) return false
        if (picture != other.picture) return false
        if (savedPicture != other.savedPicture) return false
        if (orderComponentsId != other.orderComponentsId) return false
        if (sortNum != other.sortNum) return false
        if (personCount != other.personCount) return false
        if (compositeModelId != other.compositeModelId) return false
        if (price != other.price) return false
        if (novelty != other.novelty) return false
        if (isRecommended != other.isRecommended) return false

        return true
    }

    override fun hashCode(): Int {
        var result = itemId.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + size.hashCode()
        result = 31 * result + count
        result = 31 * result + (categoryId?.hashCode() ?: 0)
        result = 31 * result + (picture?.hashCode() ?: 0)
        result = 31 * result + (savedPicture?.hashCode() ?: 0)
        result = 31 * result + (orderComponentsId?.hashCode() ?: 0)
        result = 31 * result + sortNum
        result = 31 * result + personCount
        result = 31 * result + (compositeModelId?.hashCode() ?: 0)
        result = 31 * result + price.hashCode()
        result = 31 * result + novelty.hashCode()
        result = 31 * result + isRecommended.hashCode()
        return result
    }
}
