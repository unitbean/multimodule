package com.genius.repository.database.dao

import androidx.room.*
import com.genius.core.renew
import com.genius.repository.database.models.ACTIVE
import com.genius.repository.database.models.BLOCK
import com.genius.repository.database.models.ItemModel
import com.genius.repository.database.models.ItemStatus
import com.genius.repository.di.ItemResponse
import java.sql.SQLException
import java.util.ArrayList

/**
 * Created by romanbernikov on 26.07.16.
 */
@Dao
interface ItemDAO {

    /**
     * Get all items
     * @return
     * @throws SQLException
     */
    @Throws(SQLException::class)
    @Query("SELECT * FROM items")
    fun getAll(): List<ItemModel>

    /**
     * Get all active items
     * @return
     * @throws SQLException
     */
    @Throws(SQLException::class)
    @Query("SELECT * FROM items WHERE status == :status")
    fun getItemsWithStatus(@ItemStatus status: String = ACTIVE): List<ItemModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(model: ItemModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(models: List<ItemModel>)

    @Query("UPDATE items SET status = :status")
    fun updateStatusForAll(status: String): Int

    @Transaction
    fun updateFromResponse(result: List<ItemResponse>) {
        updateStatusForAll(BLOCK)

        val models = findByItemsIds(result.map { it.id })

        for (item in result) {

            val model = models.firstOrNull { it.itemId == item.id } ?: ItemModel()

            val prices = ArrayList<Double?>()

            if (item.ingredients.isNotEmpty() && item.ingredients[0].prices.isNotEmpty()) {
                val priceMaxCode = item.ingredients[0].prices.maxBy { it.code }?.code?.plus(1) ?: 1  // берет максимальный код

                val priceList = MutableList(priceMaxCode) { index ->
                    return@MutableList item.ingredients[0].prices.firstOrNull { it.code == index }?.price
                }
                prices.renew(priceList)
            }

            model.itemId = item.id
            model.title = item.title
            model.sortNum = item.sortNum
            model.description = item.description
            model.size = item.size
            model.count = item.count
            model.categoryId = item.category
            model.orderComponentsId = item.orderComponents
            model.picture = item.picture
            model.status = item.status
            model.price = prices
            model.personCount = item.personCount
            model.isRecommended = item.isRecommended
            model.novelty = item.isNovelty

            insert(model)
        }
    }

    /**
     * find item by id
     * @param id itemId
     * @return item
     */
    @Throws(SQLException::class)
    @Query("SELECT * FROM items WHERE itemId == :id")
    fun findByItemId(id: String): ItemModel?

    /**
     * find items by ids
     * @param ids itemId
     * @return items
     */
    @Throws(SQLException::class)
    @Query("SELECT * FROM items WHERE itemId IN(:ids)")
    fun findByItemIds(ids: List<String>): List<ItemModel>

    /**
     * Получает из модели по переданному листу
     * @param ids лист с идентификаторами
     * @return лист [DescriptionItemViewModel]
     */
    @Throws(SQLException::class)
    @Query("SELECT * FROM items WHERE itemId IN(:ids)")
    fun findByItemsIds(ids: List<String>): List<ItemModel>

    /**
     * find active items by category id
     * @param id category id
     * @return list of items
     */
    @Throws(SQLException::class)
    @Query("SELECT * FROM items WHERE categoryId IN(:id) AND status == :status")
    fun findByCategoryId(id: String, @ItemStatus status: String = ACTIVE): List<ItemModel>
}
