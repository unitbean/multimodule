package com.genius.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.genius.repository.database.dao.ItemDAO
import com.genius.repository.database.models.ItemModel

@Database(
    entities = [ItemModel::class],
    version = ItemsDatabase.VERSION)
@TypeConverters(SVConverters::class)
abstract class ItemsDatabase : RoomDatabase() {

    abstract val itemDAO: ItemDAO

    companion object {
        internal const val VERSION = 1

        private const val DATABASE_NAME = "multimodule.db"

        private var INSTANCE: ItemsDatabase? = null

        fun getDatabase(context: Context): ItemsDatabase {
            synchronized(ItemsDatabase::class.java) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, ItemsDatabase::class.java, DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE ?: throw IllegalStateException("Room database isn't initialized")
        }
    }
}