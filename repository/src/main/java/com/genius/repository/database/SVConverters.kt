package com.genius.repository.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SVConverters {

    private val gson = Gson()

    @TypeConverter
    fun fromListToString(list: List<String>): String = gson.toJson(list)

    @TypeConverter
    fun fromStringToList(source: String): List<String> = gson.fromJson(source, object : TypeToken<List<String>>() {}.type)

    @TypeConverter
    fun fromPricesToString(prices: List<Double>): String = gson.toJson(prices)

    @TypeConverter
    fun fromStringToPrices(source: String): List<Double> = gson.fromJson(source, object : TypeToken<List<Double>>() {}.type)
}