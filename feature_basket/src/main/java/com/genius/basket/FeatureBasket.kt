package com.genius.basket

import com.genius.repository.IFeatureBasketRepository
import com.genius.repository.database.models.ItemModel

class FeatureBasket(private val repository: IFeatureBasketRepository) {

    val basketItems: MutableList<BasketItem> = ArrayList()
    val savedItems: MutableList<ItemModel> = ArrayList()

    suspend fun initialize() {
        if (savedItems.isNotEmpty()) return
        repository.loadAllItemsAndSave()
        basketItems.clear()
        savedItems.clear()
        savedItems.addAll(repository.getAllItems())
    }

    fun addToBasket(itemId: String) {
        val item = savedItems.firstOrNull { it.itemId == itemId } ?: return

        basketItems.firstOrNull { it.item.itemId == itemId }?.let { updatedItem ->
            val itemForUpdatePosition = basketItems.indexOf(updatedItem)
            val currentCount = basketItems[itemForUpdatePosition].count
            basketItems[itemForUpdatePosition] = basketItems[itemForUpdatePosition].copy(count = currentCount + 1)
        } ?: basketItems.add(BasketItem(item, 1))
    }

    fun removeFromBasket(itemId: String) {
        basketItems.firstOrNull { it.item.itemId == itemId }?.let { updatedItem ->
            val itemForUpdatePosition = basketItems.indexOf(updatedItem)
            val currentCount = basketItems[itemForUpdatePosition].count
            if (currentCount == 1) {
                basketItems.remove(updatedItem)
            } else {
                basketItems[itemForUpdatePosition] = basketItems[itemForUpdatePosition].copy(count = currentCount - 1)
            }
        }
    }

    fun getCount(itemId: String): Int {
        return basketItems.firstOrNull { it.item.itemId == itemId }?.count ?: 0
    }

    fun getTotalBasketPrice(): Double {
        return basketItems.sumByDouble { (it.item.price.filterNotNull().firstOrNull() ?: 0.0) * it.count }
    }

    data class BasketItem(val item: ItemModel,
                          val count: Int)
}