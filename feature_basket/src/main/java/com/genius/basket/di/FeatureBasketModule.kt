package com.genius.basket.di

import com.genius.basket.FeatureBasket
import com.genius.repository.IFeatureBasketRepository
import com.genius.repository.RepositoryDI
import dagger.Module
import dagger.Provides

@Module
class FeatureBasketModule {

    @Provides
    @BasketScope
    fun provideBasketRepository(): IFeatureBasketRepository {
        return RepositoryDI.repositoryComponent.getFeatureBasketRepository()
    }

    @Provides
    @BasketScope
    fun provideFeatureBasket(repository: IFeatureBasketRepository): FeatureBasket {
        return FeatureBasket(repository)
    }
}