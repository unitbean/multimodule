package com.genius.basket.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class BasketScope