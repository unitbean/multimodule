package com.genius.multimodule

import android.app.Application
import com.genius.core.Core
import com.genius.multimodule.di.components.*

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Core.context = applicationContext
        appComponent = DaggerAppComponent.builder()
//            .context(applicationContext)
            .build()
    }

    companion object {
        lateinit var appComponent: AppComponent

        private var featureJokeSubcomponent: JokeSubcomponent? = null
        private var featureBasketSubcomponent: BasketFeatureSubcomponent? = null

        fun getFeatureJokeSubcomponent(): JokeSubcomponent {
            if (featureJokeSubcomponent == null) {
                featureJokeSubcomponent = appComponent.featureJoke()
            }

            return featureJokeSubcomponent ?: throw IllegalStateException("$featureJokeSubcomponent must be initialized")
        }

        fun removeFeatureJokeSubcomponent() {
            featureJokeSubcomponent = null
        }

        fun getFeatureBasketSubcomponent(): BasketFeatureSubcomponent {
            if (featureBasketSubcomponent == null) {
                featureBasketSubcomponent = appComponent.basketFeature()
            }

            return featureBasketSubcomponent ?: throw IllegalStateException("$featureBasketSubcomponent must be initialized")
        }

        fun removeFeatureBasketSubcomponent() {
            featureBasketSubcomponent = null
        }
    }
}