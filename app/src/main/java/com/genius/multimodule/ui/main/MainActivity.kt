package com.genius.multimodule.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.genius.joke.FeatureRandomJoke
import com.genius.multimodule.BaseApplication
import com.genius.multimodule.R
import com.genius.multimodule.ui.basket.BasketFragment
import com.genius.multimodule.ui.catalog.CatalogFragment
import com.genius.multimodule.ui.joke.JokeFragment
import com.genius.multimodule.utils.FragmentNavigation
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(), FragmentNavigation {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (supportFragmentManager.primaryNavigationFragment == null) {
            attachFragment(R.id.fl_main_container, JokeFragment(), JokeFragment.TAG)
            setTitle(R.string.menu_title_joke)
        }

        bnv_main_navigation.setOnNavigationItemSelectedListener {
            return@setOnNavigationItemSelectedListener when (it.itemId) {
                R.id.menu_basket -> {
                    attachFragment(R.id.fl_main_container, BasketFragment(), BasketFragment.TAG)
                    setTitle(R.string.menu_title_basket)
                    true
                }
                R.id.menu_catalog -> {
                    attachFragment(R.id.fl_main_container, CatalogFragment(), CatalogFragment.TAG)
                    setTitle(R.string.menu_title_catalog)
                    true
                }
                R.id.menu_joke -> {
                    attachFragment(R.id.fl_main_container, JokeFragment(), JokeFragment.TAG)
                    setTitle(R.string.menu_title_joke)
                    true
                }
                else -> false
            }
        }
    }
}
