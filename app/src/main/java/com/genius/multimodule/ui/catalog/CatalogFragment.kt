package com.genius.multimodule.ui.catalog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.genius.basket.FeatureBasket
import com.genius.multimodule.BaseApplication
import com.genius.multimodule.R
import com.ub.utils.base.BaseClickListener
import kotlinx.android.synthetic.main.fragment_catalog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CatalogFragment : Fragment(), CoroutineScope by CoroutineScope(Dispatchers.Main), BaseClickListener {

    @Inject lateinit var basketFeature: FeatureBasket

    private val adapter: CatalogRVAdapter by lazy { CatalogRVAdapter(basketFeature).apply {
        this.listener = this@CatalogFragment
        this.updateSync(basketFeature.savedItems.map { BasketViewModel(it, basketFeature.getCount(it.itemId)) })
    } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BaseApplication.getFeatureBasketSubcomponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_catalog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_catalog_items.adapter = adapter
        val divider = DividerItemDecoration(view.context, RecyclerView.VERTICAL)
        rv_catalog_items.addItemDecoration(divider)
        rv_catalog_items.itemAnimator = null

        launch {
            withContext(Dispatchers.IO) { basketFeature.initialize() }

            adapter.updateSync(basketFeature.savedItems.map { BasketViewModel(it, basketFeature.getCount(it.itemId)) })
        }
    }

    override fun onClick(view: View, position: Int) {
        when (view.id) {
            R.id.iv_item_add -> {
                basketFeature.addToBasket(basketFeature.savedItems[position].itemId)
                adapter.updateSync(basketFeature.savedItems.map { BasketViewModel(it, basketFeature.getCount(it.itemId)) })
            }
            R.id.iv_item_remove -> {
                basketFeature.removeFromBasket(basketFeature.savedItems[position].itemId)
                adapter.updateSync(basketFeature.savedItems.map { BasketViewModel(it, basketFeature.getCount(it.itemId)) })
            }
        }
    }

    companion object {
        const val TAG = "CatalogFragment"
    }
}