package com.genius.multimodule.ui.basket

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.genius.basket.FeatureBasket
import com.genius.multimodule.R
import com.ub.utils.base.DiffUtilAdapter
import com.ub.utils.base.DiffViewHolder
import kotlinx.android.synthetic.main.rv_basket_item.view.*

class BasketRVAdapter(private val featureBasket: FeatureBasket) : DiffUtilAdapter<BasketViewModel, BasketRVAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder = ItemViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.rv_basket_item, parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = featureBasket.basketItems.size

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val title: TextView = view.tv_item_title
        private val count: TextView = view.tv_item_count
        private val add: ImageView = view.iv_item_add
        private val remove: ImageView = view.iv_item_remove

        init {
            add.setOnClickListener(this)
            remove.setOnClickListener(this)
        }

        fun bind(position: Int) {
            val basketModel = featureBasket.basketItems[position]
            title.text = basketModel.item.title
            count.text = basketModel.count.toString()
        }

        override fun onClick(v: View) {
            listener?.onClick(v, adapterPosition)
        }
    }
}

data class BasketViewModel(val item: FeatureBasket.BasketItem) : DiffViewHolder {
    override fun getDiffId(): Int = item.hashCode()
}