package com.genius.multimodule.ui.catalog

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.genius.basket.FeatureBasket
import com.genius.multimodule.R
import com.genius.repository.database.models.ItemModel
import com.ub.utils.base.DiffUtilAdapter
import com.ub.utils.base.DiffViewHolder
import kotlinx.android.synthetic.main.rv_basket_item.view.*

class CatalogRVAdapter(private val featureBasket: FeatureBasket) : DiffUtilAdapter<BasketViewModel, CatalogRVAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder = ItemViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.rv_basket_item, parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = featureBasket.savedItems.size

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val title: TextView = view.tv_item_title
        private val count: TextView = view.tv_item_count
        private val add: ImageView = view.iv_item_add
        private val remove: ImageView = view.iv_item_remove

        init {
            add.setOnClickListener(this)
            remove.setOnClickListener(this)
        }

        fun bind(position: Int) {
            val item = featureBasket.savedItems[position]
            title.text = item.title
            count.text = featureBasket.getCount(item.itemId).toString()
        }

        override fun onClick(v: View) {
            listener?.onClick(v, adapterPosition)
        }
    }
}

data class BasketViewModel(val item: ItemModel, val count: Int) : DiffViewHolder {
    override fun getDiffId(): Int = item.hashCode()
}