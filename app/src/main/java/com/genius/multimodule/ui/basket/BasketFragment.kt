package com.genius.multimodule.ui.basket

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.genius.basket.FeatureBasket
import com.genius.multimodule.BaseApplication
import com.genius.multimodule.R
import com.ub.utils.base.BaseClickListener
import kotlinx.android.synthetic.main.fragment_basket.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class BasketFragment : Fragment(), CoroutineScope by CoroutineScope(Dispatchers.Main), BaseClickListener {

    @Inject lateinit var basketFeature: FeatureBasket

    private val adapter: BasketRVAdapter by lazy { BasketRVAdapter(basketFeature).apply {
        this.listener = this@BasketFragment
        this.updateSync(basketFeature.basketItems.map { BasketViewModel(it) })
    } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BaseApplication.getFeatureBasketSubcomponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_basket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_basket_items.adapter = adapter
        val divider = DividerItemDecoration(view.context, RecyclerView.VERTICAL)
        rv_basket_items.addItemDecoration(divider)
        rv_basket_items.itemAnimator = null

        launch {
            withContext(Dispatchers.IO) { basketFeature.initialize() }

            adapter.updateSync(basketFeature.basketItems.map { BasketViewModel(it) })
            activity?.title = basketFeature.getTotalBasketPrice().toString()
        }
    }

    override fun onClick(view: View, position: Int) {
        when (view.id) {
            R.id.iv_item_add -> {
                basketFeature.addToBasket(basketFeature.basketItems[position].item.itemId)
                adapter.updateSync(basketFeature.basketItems.map { BasketViewModel(it) })
                activity?.title = basketFeature.getTotalBasketPrice().toString()
            }
            R.id.iv_item_remove -> {
                basketFeature.removeFromBasket(basketFeature.basketItems[position].item.itemId)
                adapter.updateSync(basketFeature.basketItems.map { BasketViewModel(it) })
                activity?.title = basketFeature.getTotalBasketPrice().toString()

            }
        }
    }

    companion object {
        const val TAG = "BasketFragment"
    }
}