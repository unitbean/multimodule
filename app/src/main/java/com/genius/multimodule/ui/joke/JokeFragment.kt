package com.genius.multimodule.ui.joke

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.genius.joke.FeatureRandomJoke
import com.genius.multimodule.BaseApplication
import com.genius.multimodule.R
import kotlinx.android.synthetic.main.fragment_joke.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class JokeFragment : Fragment(), CoroutineScope by CoroutineScope(Dispatchers.Main) {

    @Inject
    lateinit var feature1: FeatureRandomJoke

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BaseApplication.getFeatureJokeSubcomponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_joke, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_generate.setOnClickListener {
            launch {
                tv_text.text = withContext(Dispatchers.IO) { feature1.fetchJoke() }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        BaseApplication.removeFeatureJokeSubcomponent()
    }

    companion object {
        const val TAG = "JokeFragment"
    }
}