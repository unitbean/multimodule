package com.genius.multimodule.utils

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/**
 * Суть данного интерфейса в том, что только те, кто явно его реализует, могут изменять навигацию
 * фрагментов на экране. Основные методы реализованы через методы-расширения, которые будут доступны
 * только в соответствующих классах, что делает зависимость навигации явной и не требует еще методов
 * в классе Utils
 *
 * Так как реализации данных методов уже указаны, то не требуется их переопределять в реализующих этот
 * интерфейс, классах. Таким образом, реализуя этот интерфейс, классу гарантированно добавляется функционал
 * этого интерфейса
 */
interface FragmentNavigation {

    /**
     * Метод-расширение для открытия переданного фрагмента на экране активности
     *
     * Данный метод получает по переданному тэгу [tag] фрагмент из [androidx.fragment.app.FragmentManager]
     * Если найденный фрагмент != null и [Fragment.isAdded], то на него делается [FragmentTransaction.show],
     * который делает его видимым. В противном случае инстанс фрагмента, переданный в [fragmentInstance]
     * добавляется в [androidx.fragment.app.FragmentManager]
     *
     * Так как параметр [tag] nullable, если в него не передавать значение, то каждый раз будет создаваться
     * новый инстанс фрагмента [fragmentInstance] и добавляться в [androidx.fragment.app.FragmentManager],
     * что не очень хорошо с точки зрения пользовательского опыта и оптимизации работы приложения
     *
     * Итерируются все фрагменты, содержащиеся в [androidx.fragment.app.FragmentManager.getFragments]
     * с заданием каждому из них [FragmentTransaction.hide], если это не искомый фрагмент и [Fragment.isAdded]
     *
     * Если задан флаг [isAddToBackStack], то применяется метод [FragmentTransaction.addToBackStack],
     * добавляющий [FragmentTransaction] в стак возврата.
     * По-умолчанию данный флаг стоит в позиции [java.lang.Boolean.FALSE]
     *
     * [FragmentTransaction] задается эффект перехода [FragmentTransaction.TRANSIT_FRAGMENT_FADE],
     * фрагмент устанавливается как [FragmentTransaction.setPrimaryNavigationFragment],
     * задается флаг оптимизации [FragmentTransaction.setReorderingAllowed] и транзакция добавляется
     * в очередь выполнения [androidx.fragment.app.FragmentManager]
     *
     * @param containerId - идентификатор контейнера, в который будет помещён фрагмент
     * @param fragmentInstance - инстанс фрагмента для создания его, если ранее создан не был
     * @param tag - метка фрагмента, по которой можно будет найти фрагмент среди уже созданных
     * @param isAddToBackStack - флаг, говорящий о том, добавлять в backStack [fragmentInstance] или нет
     */
    fun AppCompatActivity.attachFragment(@IdRes containerId: Int, fragmentInstance: Fragment, tag: String?, isAddToBackStack: Boolean = false) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment = supportFragmentManager.findFragmentByTag(tag)
        if (fragment == null) {
            fragment = fragmentInstance
            fragmentTransaction.add(containerId, fragment, tag)
        } else if (fragment.isAdded) {
            fragmentTransaction.show(fragment)
        }

        for (frag in supportFragmentManager.fragments) {
            if (frag != fragment && frag.isAdded) {
                fragmentTransaction.hide(frag)
            }
        }

        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag)
        }

        fragmentTransaction
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .setPrimaryNavigationFragment(fragment)
            .setReorderingAllowed(true)
            .commit()
    }

    /**
     * Метод-расширение для открытия переданного фрагмента на экране из другого фрагмента
     *
     * Метод [Fragment.requireActivity] проверяет, что данный фрагмент проассоциирован с [androidx.fragment.app.FragmentActivity]
     * и если нет, то выбрасывается стандартное исключение в этом методе [java.lang.IllegalStateException]
     *
     * Проверяется, что [androidx.fragment.app.FragmentActivity], к которой прикреплен фрагмент, на который вызывают
     * эту функцию, является [AppCompatActivity], и если да, то запускается метод [attachFragment].
     * Если же проверка не проходит, то происходит возврат из функции
     *
     * Сигнатура метода полностью совпадает с [attachFragment]
     */
    fun Fragment.attachFragment(@IdRes containerId: Int, fragmentInstance: Fragment, tag: String?, isAddToBackStack: Boolean = false) {
        if (requireActivity() !is AppCompatActivity) return

        (requireActivity() as AppCompatActivity).attachFragment(containerId, fragmentInstance, tag, isAddToBackStack)
    }

    fun Fragment.attachChildFragment(@IdRes containerId: Int, fragmentInstance: Fragment, tag: String?, isAddToBackStack: Boolean = false) {
        val fragmentTransaction = childFragmentManager.beginTransaction()

        var fragment = childFragmentManager.findFragmentByTag(tag)
        if (fragment == null) {
            fragment = fragmentInstance
            fragmentTransaction.add(containerId, fragment, tag)
        } else if (fragment.isAdded) {
            fragmentTransaction.show(fragment)
        }

        for (frag in childFragmentManager.fragments) {
            if (frag != fragment && frag.isAdded) {
                fragmentTransaction.hide(frag)
            }
        }

        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag)
        }

        fragmentTransaction
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .setPrimaryNavigationFragment(fragment)
            .setReorderingAllowed(true)
            .commit()
    }
}