package com.genius.multimodule.di.components

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component
interface AppComponent {

//    val context: Context

    fun featureJoke(): JokeSubcomponent
    fun basketFeature(): BasketFeatureSubcomponent

    @Component.Builder
    interface Builder {
//        @BindsInstance
//        fun context(context: Context): Builder
        fun build(): AppComponent
    }
}