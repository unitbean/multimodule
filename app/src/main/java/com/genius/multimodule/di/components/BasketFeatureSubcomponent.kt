package com.genius.multimodule.di.components

import com.genius.basket.di.BasketScope
import com.genius.basket.di.FeatureBasketModule
import com.genius.multimodule.ui.basket.BasketFragment
import com.genius.multimodule.ui.catalog.CatalogFragment
import dagger.Subcomponent

@BasketScope
@Subcomponent(modules = [FeatureBasketModule::class])
interface BasketFeatureSubcomponent {

    fun inject(inject: BasketFragment)
    fun inject(inject: CatalogFragment)
}