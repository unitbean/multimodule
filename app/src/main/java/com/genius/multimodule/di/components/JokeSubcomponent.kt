package com.genius.multimodule.di.components

import com.genius.joke.di.FeatureJokeModule
import com.genius.joke.di.FeatureRandomJokeScope
import com.genius.multimodule.ui.joke.JokeFragment
import dagger.Subcomponent

@FeatureRandomJokeScope
@Subcomponent(modules = [FeatureJokeModule::class])
interface JokeSubcomponent {
    fun inject(inject: JokeFragment)
}